using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace MyApplication
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void OpenImage(object sender, EventArgs e)
        {
            if ( ofd.ShowDialog(this)!=DialogResult.OK )    return;
            try
            {
                using ( Bitmap img = new Bitmap(ofd.FileName) )
                {
                    picBox.Image = (Image)img.Clone();
                    picBox.Size = img.Size;
                }
            }
            catch (Exception ex)
            {
                Program.ShowException(this,ex);
            }
        }

        private void SaveImage(object sender, EventArgs e)
        {
            if (picBox.Image == null) return;
            if (sfd.ShowDialog(this) != DialogResult.OK) return;
            try
            {
                ImageFormat imageFormat = ImageFormat.Png;
                switch (sfd.FilterIndex)
                {
                    case 1:
                        imageFormat = ImageFormat.Tiff;
                        break;
                    case 2:
                        imageFormat = ImageFormat.Jpeg;
                        break;
                }
                picBox.Image.Save( sfd.FileName, imageFormat );
            }
            catch (Exception ex)
            {
                Program.ShowException(this, ex);
            }
        }

        private void Exit(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EditSourceCode(object sender, EventArgs e)
        {
            try
            {
                rtc.Dlg_EditCode();
            }
            catch (Exception ex)
            {
                Program.ShowException(this, ex);
            }
        }

        private void RunCode(object sender, EventArgs e)
        {
            rtc.MainFuncName = "TestFunc";
            picBox.Image = rtc.RunCode( new Bitmap(picBox.Image) );
        }

    }
}