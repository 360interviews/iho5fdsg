using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Reflection;
using System.CodeDom;
using System.Collections.Specialized;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.IO;

namespace MyApplication
{
    public partial class CodeEditorForm : Form
    {
        public string codeFilePath = Application.StartupPath + "\\temp";
        public CodeEditorForm()
        {
            InitializeComponent();
        }

        private void EditReferances(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
        public Type scClassType = null;
        CompilerResults compilerResults = null;
        private void CompileCode(object sender, EventArgs e)
        {
            try
            {
                compilerResults = null;
                ErrsComboBox.Items.Clear();
                Assembly scAssembly = null;
                CompilerParameters compilerParams = new CompilerParameters();
                compilerParams.ReferencedAssemblies.Add("system.windows.forms.dll");
                compilerParams.ReferencedAssemblies.Add("system.drawing.dll");
				compilerParams.GenerateExecutable	= false;
				compilerParams.GenerateInMemory		= false;
				compilerParams.OutputAssembly		= Path.GetTempFileName();
				#if DEBUG
	            compilerParams.IncludeDebugInformation = true;
				#endif
                CodeDomProvider codeDomProvider = CSharpCodeProvider.CreateProvider("csharp");
                compilerResults = codeDomProvider.CompileAssemblyFromSource(compilerParams, SourceCode.Text);		
                
				if (compilerResults.Errors.Count>0)
				{
					throw new Exception("Error in your source code");
				}
				else
				{
					scAssembly = Assembly.LoadFrom(compilerResults.PathToAssembly);
					scClassType= scAssembly.GetType("CTestClass", true, true);
				}
                ErrsComboBox.Items.Add( "OK" );
                ErrsComboBox.SelectedIndex = 0;
			}
			catch (Exception ex)
			{
                Program.ShowException(this, ex);
				foreach ( CompilerError err in compilerResults.Errors )
				{
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Line: (");
                    sb.Append(err.Line);
                    sb.Append(",");
                    sb.Append(err.Column);
                    sb.Append(")\t\t");
                    sb.Append(err.ErrorText);
                    ErrsComboBox.Items.Add(sb.ToString());
                    //MessageBox.Show( sb.ToString() );
				}
                if (ErrsComboBox.Items.Count>=1)    ErrsComboBox.SelectedIndex = 0;
			}

        }

        private void EditRunTimeOptions(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void SelectErr(object sender, EventArgs e)
        {
            //if (compilerResults == null) return;
            //CompilerError err = compilerResults.Errors[ErrsComboBox.SelectedIndex];
            //SourceCode.
        }

        private void CodeEditorForm_Load(object sender, EventArgs e)
        {
            try
            {
                StreamReader sr = new StreamReader(codeFilePath);
                SourceCode.Text = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex) { Program.ShowException(null, ex); }
        }

        private void CodeEditorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                StreamWriter sw = new StreamWriter(codeFilePath);
                sw.Write(SourceCode.Text);
                sw.Close();
            }
            catch (Exception ex) { Program.ShowException(null, ex); }
        }
    }
}