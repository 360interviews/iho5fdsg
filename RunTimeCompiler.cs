using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using System.Reflection;
using System.CodeDom;
//using System.CodeDom.Compiler;
//using Microsoft.CSharp;
using System.Collections.Specialized;

namespace MyApplication
{
    public class RunTimeCompiler
    {
        private Type m_rtType = null;
        private string m_mainFuncName;

        public String MainFuncName
        {
            get { return m_mainFuncName;  }
            set { m_mainFuncName = value; }
        }

        public void Dlg_EditCode()
        {
            CodeEditorForm dlg = new CodeEditorForm();
            dlg.ShowDialog();
            m_rtType = dlg.scClassType;
        }

        public Bitmap RunCode(Bitmap img)
        {
            try
            {
                object[] args = { img };
                return (Bitmap)m_rtType.InvokeMember(
                    m_mainFuncName,
                    (BindingFlags)(
                       BindingFlags.InvokeMethod |
                       BindingFlags.DeclaredOnly |
                       BindingFlags.Public |
                       BindingFlags.NonPublic |
                       BindingFlags.Static
                   ),
                   null, null, args);
            }
            catch (Exception ex)
            {
                Program.ShowException(null, ex.InnerException);
                return null;
            }
        }
    }
}
